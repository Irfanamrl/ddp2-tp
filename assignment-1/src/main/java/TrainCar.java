public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;
    // TODO Complete me!

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }
    public void setCat(WildCat cat){
        this.cat = cat;
    }

    public double computeTotalWeight() {
        if (next == null){
            return EMPTY_WEIGHT + cat.weight;
        }
        else{
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (next == null){
            return cat.computeMassIndex();
        }
        else{
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if(next == null){
            System.out.print("(" + cat.name + ")");
        }
        else{
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
