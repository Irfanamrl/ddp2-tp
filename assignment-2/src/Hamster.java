public class Hamster extends Animal {
    
    public Hamster(String nama, int panjang) {
        super(nama, panjang);
        this.wild = false;
        this.spesies = "hamster";
    }

    public void gnaw() {
        System.out.println(nama + " makes a voice: Ngkkrit.. Ngkkrrriiit");
        System.out.println("Back to the office!");
    }

    public void hamsterWheel() {
        System.out.println(nama + " makes a voice: trrr.... trrr...");
        System.out.println("Back to the office!");
    }

    public void activity(int n) {
        if (n == 1) {
            gnaw();
        }
        else if (n == 2) {
            hamsterWheel();
        }
        else {
            System.out.println("You do nothing...");
        }
    }
}