public class Parrot extends Animal {

    public Parrot(String nama, int panjang) {
        super(nama, panjang);
        this.wild = false;
        this.spesies = "parrot";
    }

    public void fly() {
        System.out.println("Parrot "+nama+" flies!");
        System.out.println(nama + " makes a voice: FLYYYY.....");
        System.out.println("Back to the office!");
    }

    public void conversation(String word) {
        System.out.println(nama + " says: "+ word.toUpperCase());
        System.out.println("Back to the office!");
    }

    public void noOne() {
        System.out.println(nama +" says: HM?");
        System.out.println("Back to the office");
    }
}