public class Animal {
    String nama;
    int panjang;
    String spesies;
    boolean wild;

    public Animal(String nama, int panjang) {
        this.nama = nama;
        this.panjang = panjang;
    }

    public String getNama() {
        return this.nama;
    }

    public int getPanjang() {
        return this.panjang;
    }

    public boolean isWild() {
        return this.wild;
    }

    public String getSpesies() {
        return this.spesies;
    }

    public void activity(int n) {
        return;
    }
}