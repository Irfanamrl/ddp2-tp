public class Eagle extends Animal {

    public Eagle(String nama, int panjang) {
        super(nama, panjang);
        this.wild = true;
        this.spesies = "eagle";
    }

    public void fly() {
        System.out.println(nama + " make a voice : Kwakk...");
        System.out.println("You hurt!");
        System.out.println("Back to the office!");
    }

    public void activity(int n) {
        if (n == 1) {
            fly();
        }
        else {
            System.out.println("You do nothing...");
        }
    }
}