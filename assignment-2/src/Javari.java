import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Javari {

    public static void main(String[] args) {
        int totalCat;
        int totalLion;
        int totalEagle;
        int totalParrot;
        int totalHamster;

        System.out.println("Welcome to Javari Park\n" +
                "Input the number of animals");
        Scanner input = new Scanner(System.in);

        System.out.print("Cat: ");
        totalCat = input.nextInt();
        String[][] arrCat = printInformation(totalCat, "cat");
        Animal[] objCat = toAnimal(arrCat, "cat");


        System.out.print("Lion: ");
        totalLion = input.nextInt();
        String[][] arrLion = printInformation(totalLion, "lion");
        Animal[] objLion = toAnimal(arrLion, "lion");

        System.out.print("Eagle: ");
        totalEagle = input.nextInt();
        String[][] arrEagle = printInformation(totalEagle, "eagle");
        Animal[] objEagle = toAnimal(arrEagle, "eagle");

        System.out.print("Parrot: ");
        totalParrot = input.nextInt();
        String[][] arrParrot = printInformation(totalParrot, "parrot");
        Animal[] objParrot = toAnimal(arrParrot, "parrot");

        System.out.print("Hamster: ");
        totalHamster = input.nextInt();
        String[][] arrHamster = printInformation(totalHamster, "hamster");
        Animal[] objHamster = toAnimal(arrHamster, "hamster");

        System.out.println("Animals have been successfully recorded!\n"
                + "=============================================");

        Animal[][] objAnimals= {objCat, objLion, objEagle, objParrot, objHamster};
        ArrayList<Cage[]> cages = makeCage(objAnimals);
        ArrayList<ListOfLevels> packOfCage = packingAndPrint(cages);

        System.out.println("ANIMALS NUMBER:");
        System.out.println("cat:"+totalCat);
        System.out.println("lion:"+totalLion);
        System.out.println("parrot:"+totalParrot);
        System.out.println("eagle:"+totalEagle);
        System.out.println("hamster:"+totalHamster);

        System.out.println("\n=============================================");

        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            String[] tempAnimal = {"cat", "eagle", "hamster", "parrot", "lion"};
            int animal = input.nextInt();
            if (animal == 99) {
                System.out.println("Program will exit");
                break;
            }
            else {
                int sum = 0;
                int index = 0;
                switch(animal) {
                    case 1:
                        sum = totalCat;
                        index = 0;
                        break;
                    case 2:
                        sum = totalEagle;
                        index = 2;
                        break;
                    case 3:
                        sum = totalHamster;
                        index = 4;
                        break;
                    case 4:
                        index = 3;
                        sum = totalParrot;
                        break;
                    case 5:
                        sum = totalLion;
                        index = 1;
                        break;
                    default:
                        System.out.println("Wrong command");
                        break;
                }
                if (sum >0) {
                    System.out.printf("Mention the name of %s you want to visit: \n", tempAnimal[animal - 1]);        
                    String tempName = input.nextLine();
                    tempName = input.nextLine();
                    tempName.toLowerCase();
                    boolean chosen = false;
                    Animal animalChosen = null;
                    for (Animal binatang : objAnimals[index]) {
                        if(binatang.getNama().equals(tempName)) {
                            chosen = true;
                            animalChosen = binatang;
                        }
                    }
                    if (chosen) {
                        System.out.println("You are visiting " + animalChosen.getNama()
                        +" (" + tempAnimal[animal - 1] + ") now, what would you like to do?");

                        switch (index) {
                            case 0:
                                System.out.println("1: Brush the fur 2: Cuddle");
                                int choose = input.nextInt();
                                animalChosen.activity(choose);
                                break;
                            case 2:
                                System.out.println("1: Order to fly");
                                int choose1 = input.nextInt();
                                animalChosen.activity(choose1);
                                break;
                            case 4:
                                System.out.println("1: See it gawning 2: Order to run in"
                                +" the hamster wheel");
                                int choose2 = input.nextInt();
                                animalChosen.activity(choose2);
                                break;
                            case 3:
                                System.out.println("1: Order to fly 2: Do conversation");
                                int choose3 = input.nextInt();
                                switch (choose3) {
                                    case 1:
                                        ((Parrot) animalChosen).fly();
                                        break;
                                    case 2:
                                        System.out.print("You say: ");
                                        input.nextLine();
                                        String word = input.nextLine();
                                        ((Parrot) animalChosen).conversation(word);
                                        break;
                                    default:
                                        ((Parrot) animalChosen).noOne();
                                        break;
                                }
                                animalChosen.activity(choose3);
                                break;
                            case 1:
                                System.out.println("1: See it hunting 2: Brush the mane "
                                        + "3: Disturb it");
                                int choose4 = input.nextInt();
                                animalChosen.activity(choose4);
                                break;
                            default:
                                System.out.println("You choose nothing");
                                break;
                        }
                    }
                    else {
                        System.out.println("There is no " + tempAnimal[animal - 1] + " with that name!"
                        + " Back to the office!");
                    }   
                }
            }
        }
    }

    private static String[][] printInformation(int totalAnimal, String animal) {
        Scanner input = new Scanner(System.in);
        if (totalAnimal  > 0) {
            System.out.printf("Provide the information of %s(s): \n", animal);
            String animals = input.nextLine();
            String[] animalsSplit = animals.split(",");
            String[][] splitNameLength = new String[animalsSplit.length][2];
            for (int i = 0; i < animalsSplit.length; i++) {
                splitNameLength[i] = animalsSplit[i].split("\\|");
            }
            return splitNameLength;
        }
        else {
        return null;
        }
    }

    private static Animal parseAnimal(String name, int length, String animal) {
        if (animal.equals("cat")) {
            return new Cat(name, length);
        } 
        else if (animal.equals("lion")) {
            return new Lion(name, length);
        } 
        else if (animal.equals("eagle")) {
            return new Eagle(name, length);
        } 
        else if (animal.equals("parrot")) {
            return new Parrot(name, length);
        } 
        else {
            return new Hamster(name, length);
        }
    }

    private static ListOfLevels pack(Cage[] cages, int totalAnimal) {
        int x;
        int y;
        if (totalAnimal % 3 == 2) {
            x = totalAnimal/3 + 1;
            y = 2*totalAnimal/3 + 1;
        } 
        else {
            x = totalAnimal / 3;
            y = 2 * totalAnimal / 3;
        }
        if (cages != null) {
            ListOfCages cages3 = new ListOfCages(3, Arrays.copyOfRange(cages, y, cages.length));
            ListOfCages cages2 = new ListOfCages(2, Arrays.copyOfRange(cages, x, y));
            ListOfCages cages1 = new ListOfCages(1, Arrays.copyOfRange(cages, 0, x));
            return new ListOfLevels(cages1, cages2, cages3);
        } 
        else {
            ListOfCages cages3 = new ListOfCages(3, null);
            ListOfCages cages2 = new ListOfCages(2, null);
            ListOfCages cages1 = new ListOfCages(1, null);
            return new ListOfLevels(cages1, cages2, cages3);
        }


    }

    private static ArrayList<ListOfLevels> packingAndPrint(ArrayList<Cage[]> cages) {
        ArrayList<ListOfLevels> packs = new ArrayList <>();
        for (int i = 0; i < cages.size(); i++) {
            ListOfLevels animalPack;
            if (cages.get(i) != null) {
                animalPack = pack(cages.get(i), cages.get(i).length);
            } 
            else {
                animalPack = pack(null, 0);
            }
            if (animalPack.getLevel1() != null) {
                animalPack.printArrangement();
                System.out.println();
                animalPack.afterArrangement();
                System.out.println();
            }
            packs.add(animalPack);
        }
        return packs;
    }

    private static Animal[] toAnimal(String[][] arrAnimals, String animal) {
        try {
            Animal[] objAnimal = new Animal[arrAnimals.length];
            int indexName = 0;
            int indexLength = 1;
            for (int i = 0; i < arrAnimals.length; i++) {
                if (arrAnimals[i] != null) {
                    objAnimal[i] = parseAnimal(arrAnimals[i][indexName]
                            , Integer.parseInt(arrAnimals[i][indexLength]), animal);
                } 
                else {
                    objAnimal[i] = null;
                }
            }
            return objAnimal;
        } catch (NullPointerException e) {
            return null;
        }


    }

    private static Cage[] toCage(Animal[] animals) {
        Cage[] cages = new Cage[animals.length] ;
        for (int i = 0; i < animals.length; i++) {
            cages[i] = new Cage(animals[i]);
        }
        return cages;
    }

    private static ArrayList<Cage[]> makeCage(Animal[][] objAnimals) {
        ArrayList<Cage[]> cages = new ArrayList <>();
        for (int i = 0; i < objAnimals.length; i++) {
            if (objAnimals[i] != null) {
                Cage[] animalCage = toCage(objAnimals[i]);
                cages.add(animalCage);
            } 
            else {
                Cage[] animalCage = null;
                cages.add(animalCage);
            }
        }
        return cages;
    }
}
